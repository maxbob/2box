extends Node
class_name consts

const BATTLER_NAME: String = "n"
const BATTLE_CARD: String = "c"
const BATTLE_SPRITE: String = "s"
const TARGET_PANEL: String = "p"
const BATTLE_MENU: String = "m"

const TURN_PROGRESS_MAX: float = 100.0