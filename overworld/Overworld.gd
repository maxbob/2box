extends Node2D
class_name Overworld

# Debug
var cum: float

# Overworld specifics
var currLevel: Node2D
var spawn: Node2D

var party: Party

func _ready():
	party = self.find_node("Party")
	self.transition_to("res://levels/grass1.tscn", "ne_spawn")
	
func _process(delta: float):
	cum += delta
	if cum > 3.0:
		print(OS.get_dynamic_memory_usage())
		print(Engine.get_frames_per_second())
		cum = 0
	
func transition_to(level: String, spawnNodeName: String):
	print("transitioning to %s %s" % [level, spawnNodeName])
	var prevLevel = currLevel
	if is_instance_valid(prevLevel):
		self.remove_child(prevLevel)
		call_deferred("free", prevLevel)
	
	var levelRsc = load(level)
	if !is_instance_valid(levelRsc):
		print("level resource %s failed to load" % level)
		return

	currLevel = levelRsc.instance()
	spawn = currLevel.find_node(spawnNodeName)
	if !is_instance_valid(spawn):
		print("spawn point %s failed to load" % spawnNodeName)
		return
	
	self.add_child(currLevel)
	self.move_child(currLevel, 0)
	currLevel.set_global_position(Vector2(0, 0));
	self.party.reset_position(spawn.get_global_position())
	
func on_zone_entered(bod: PhysicsBody2D, zone: Zone):
	if zone == null:
		return
	self.transition_to(zone.toLevel, zone.toSpawn);