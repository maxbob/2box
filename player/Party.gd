extends Node2D
class_name Party

const MOVE_SPEED = 160
const FOLLOW_DISTANCE = MOVE_SPEED / 4

var primaryPlayer: PlayerChar
var secondaryPlayer: PlayerChar
var camera = null

# Called when the node enters the scene tree for the first time.
func _ready(): 
	self.camera = get_node("Camera2D")
	self.secondaryPlayer = get_node("PlayerA")
	self.primaryPlayer = get_node("PlayerB")
	self.toggle_leader()

func _process(delta):
	if Input.is_action_just_pressed("switch_char"):
		self.toggle_leader()

func _physics_process(delta):
	secondaryFollow()
	primaryMove()

func _unhandled_input(event: InputEvent):
	if event is InputEventMouseMotion:
		return

# reset_position is sends the main and secondary player to a specific location
func reset_position(pos: Vector2):
	primaryPlayer.set_global_position(pos)
	secondaryPlayer.set_global_position(pos + Vector2(15,15))

#
# MOTION
#

func primaryMove():
	var mainMotion = Vector2()
	if Input.is_action_pressed("move_up"):
		mainMotion += Vector2(0, -1)
	if Input.is_action_pressed("move_down"):
		mainMotion += Vector2(0, 1)
	if Input.is_action_pressed("move_left"):
		mainMotion += Vector2(-1, 0)
	if Input.is_action_pressed("move_right"):
		mainMotion += Vector2(1, 0)
	mainMotion = mainMotion.normalized() * MOVE_SPEED
	if mainMotion != Vector2.ZERO:	
		primaryPlayer.move_and_slide(mainMotion)

func secondaryFollow():
	var primaryPlayerPos = primaryPlayer.get_global_position()
	var secondaryPlayerPos = secondaryPlayer.get_global_position()
	var playerDistance = secondaryPlayerPos.distance_to(primaryPlayerPos)
	if playerDistance > FOLLOW_DISTANCE:
		var secMotion = secondaryPlayerPos.direction_to(primaryPlayerPos) * MOVE_SPEED * (playerDistance / FOLLOW_DISTANCE - 0.5)
		secondaryPlayer.move_and_slide(secMotion)

#
# PARTY MANAGEMENT
#

func toggle_leader():
	var tmp = primaryPlayer
	primaryPlayer = secondaryPlayer
	secondaryPlayer = tmp
	camera.get_parent().remove_child(camera)
	primaryPlayer.add_child(camera)
	camera.set_global_position(primaryPlayer.get_global_position())
	self.move_child(secondaryPlayer, 0)

#
# ATTACKING
#

func print_directional_stuff(posA: Vector2, posB: Vector2):
	print ("##")
	print(posA.angle_to_point(posB))
	print(posB.angle_to_point(posA))
	print(posA.angle_to(posB))
	print(posB.angle_to(posA))
	print("##")