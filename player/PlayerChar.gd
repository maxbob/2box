extends KinematicBody2D
class_name PlayerChar

var type: String = "PlayerChar"

var skills: Array

var health: float = 250.0
var maxHealth: float = 500.0
var healthRegen: float = 10.0

var mana: float = 100.0
var maxMana: float = 100.0
var manaRegen: float = 10.0

func _ready():
	skills = []

func _process(delta: float):
	if health < maxHealth:
		health = min(health + healthRegen * delta, maxHealth)
	if mana < maxMana:
		mana = min(mana + manaRegen * delta, maxMana)
	for skill in skills:
		skill._tick(delta)

func get_texture() -> Texture:
	return $Sprite.texture