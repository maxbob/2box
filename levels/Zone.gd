extends Area2D
class_name Zone

#
# Zone is primarily a data class that contains a pointer TO the next zone that should be transitioned to
# It also contains a collision object which will report when a transition should happen
#

var toLevel: String
var toSpawn: String
var zoneName: String

func _ready():
	print("ready called in %s" % zoneName)
	var ow: Node2D = get_tree().get_root().get_node("Overworld")
	if ow != null:
		print("connecting %s to overworld" % zoneName)
		self.connect("body_entered", ow, "on_zone_entered", [self])
