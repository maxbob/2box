extends KinematicBody2D

const MOVE_SPEED = Vector2(2, 0)

var health: float = 100
var targets: Array = []

func _ready():
	get_child(2).connect("body_entered", self, "enter_aggro_ring", [])
	get_child(2).connect("body_exited", self, "exit_aggro_ring", [])

func take_dmg(dmg: float, type: int):
	var pre = health
	health -= dmg
	var post = health
	if health <= 0:
		self.queue_free()
		
func _physics_process(delta: float):
	if len(targets) > 0:
		self.move_and_collide(MOVE_SPEED.rotated(targets[0].get_global_position().angle_to_point(self.get_global_position())))
	
func enter_aggro_ring(body: PhysicsBody2D):
	if body.get("type") == "PlayerChar":
		targets.append(body)
		
func exit_aggro_ring(body: PhysicsBody2D):
	targets.erase(body)