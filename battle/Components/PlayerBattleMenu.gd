extends MarginContainer
class_name PlayerBattleMenu

const NAME_FMT: String = "%s_pbm"

signal return_to_overview

func _ready():
	$StatsAndAttacks/VBoxContainer/BackButton.connect("pressed", self, "_back_called")

func _back_called():
	emit_signal("return_to_overview")