extends Node2D
class_name BattleSprite

signal turn_ready(battler)
signal player_done(battler)

const TURN_CD: float = 3.0
const NAME_FMT: String = "%s_sprite"

# POOLS
var max_hp: int
var hp: int

var max_ap: int
var ap: int

# STATS
var weight: int
var alacrity: int
var defense: int
var ferocity: int
var acuity: int

# BATTLE CONFIG
var in_front: bool = true
var is_player: bool = false

func setup(config: Dictionary):
	pass

func is_dead() -> bool:
	return false

func get_pos() -> Array:
	return [position, z_index]

func act(friendlies: Array, enemies: Array) -> void:
	#print("act called: ", self, ", ", is_player)
	pass

func _act(friendlies: Array, enemies: Array) -> void:
	act(friendlies, enemies)
	if self.is_player:
		emit_signal("player_done", self)

# Called when the node enters the scene tree for the first time.
func _ready():
	$sprite.play()
	$turnTimer.connect("timeout", self, "_tick")
	var turn_cd = TURN_CD * (100 - alacrity) / 100
	$turnTimer.start(turn_cd)

func _flip_sprite():
	$sprite.set_flip_h(!$sprite.flip_h)

func _set_pos(pos: Vector2, z: int):
	position = pos
	z_index = z

func _tick() -> void:
	emit_signal("turn_ready", self)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
