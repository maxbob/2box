extends Position2D

var sprites: Array = []
var pos_changed: bool = false

var FrontLaneTop: Vector2
var FrontLaneBot: Vector2
var BackLaneTop: Vector2
var BackLaneBot: Vector2

func _ready():
	FrontLaneTop = $Front.get_point_position(0)
	FrontLaneBot = $Front.get_point_position(1)
	BackLaneTop = $Back.get_point_position(0)
	BackLaneBot = $Back.get_point_position(1)

func _process(delta: float):
	if pos_changed:
		_update_positions()
		pos_changed = false

func add_sprite(b: BattleSprite) -> void:
	for s in sprites:
		if b == s:
			return
	sprites.append(b)
	add_child(b)
	pos_changed = true
	

func _update_positions():
	var idx: float = 1.0
	for sprite in sprites:
		var ratio = idx / (len(sprites) + 1)
		sprite.position = FrontLaneTop + (FrontLaneBot - FrontLaneTop) * ratio
		idx += 1
