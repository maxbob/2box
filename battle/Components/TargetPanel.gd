extends PanelContainer
class_name TargetPanel

const NAME_FMT: String = "%s_tp"

func set_rb_val(val: float):
	$MarginContainer/HBoxContainer/ReadyBar.set_val(val)