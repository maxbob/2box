extends Node

class_name Battler

const MAX_PROGRESS: float = 100.0
const GROUP_FMT = "%s_%s"

# Stats
var _hp_max: float
var _rp_max: float
var _atk: int
var _def: int
var _alacrity: int

# Pre-Battle variables
var _name: String
var _sprite: Texture
var _skills: Dictionary

# In-Battle variables
var _components: Dictionary
var _hp: float
var _rp: float
var _position: int
var _imparements: Array
var _boons: Array
var _step: float
var _turn_progress: float
var _is_pc: bool = false

func init_battle(pos: int):
	_hp = _hp_max
	_rp = _rp_max
	_position = pos
	_imparements = []
	_boons = []
	_step = _calc_step_size()
	_turn_progress = MAX_PROGRESS

func set_components(c: Dictionary):
	_components = c
	if _components.get(consts.BATTLE_CARD) != null:
		_is_pc = true

func _calc_step_size() -> float:
	# todo: use stats
	return 5.0

var _print_tp = false
func _process(delta: float):
	if _turn_progress > 0:
		_turn_progress -= _step * delta
		_update_all_ready_bars()
		if _turn_progress <= 0:
			_turn_progress = 0
			_take_turn()

func _reset_turn_progress():
	_turn_progress = consts.TURN_PROGRESS_MAX
	_print_tp = true

func _update_all_ready_bars():
	var rb = _components.get(consts.TARGET_PANEL) as TargetPanel
	rb.set_rb_val(_turn_progress)
	if _is_pc:
		rb = _components.get(consts.BATTLE_CARD) as BattleCard
		rb.set_rb_val(_turn_progress)

func _take_turn():
	if _is_pc:
		var bc = _components.get(consts.BATTLE_CARD) as BattleCard
		bc.show_button()
	else: # we're dealing with an NPC
		act()
		_reset_turn_progress()
		
func act():
	pass