extends PanelContainer
class_name BattleCard

const NAME_FMT: String = "%s_bc"

signal bc_pressed

func _ready():
	$Spacer/Stack/TakeTurnButton.connect("pressed", self, "ttb_pressed")

func set_rb_val(val: float):
	$Spacer/Stack/ReadyBar.set_val(val)

func show_button():
	$Spacer/Stack/ReadyBar.visible = false
	$Spacer/Stack/TakeTurnButton.visible = true

func ttb_pressed():
	emit_signal("bc_pressed")