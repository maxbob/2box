extends MarginContainer

func _ready():
	$AHAB/AttacksButton.connect("pressed", self, "switch_to", [$AHAB, $Attacks])
	$AHAB/BuffsButton.connect("pressed", self, "switch_to", [$AHAB, $Boons])
	$AHAB/HealsButton.connect("pressed", self, "switch_to", [$AHAB, $Heals])
	$AHAB/DebuffsButton.connect("pressed", self, "switch_to", [$AHAB, $Afflictions])
	_connect_btm_buttons()

func _connect_btm_buttons():
	$Attacks/BTMContainer/BTMButton.connect("pressed", self, "switch_to", [$Attacks, $AHAB])
	$Heals/BTMContainer/BTMButton.connect("pressed", self, "switch_to", [$Heals, $AHAB])
	$Afflictions/BTMContainer/BTMButton.connect("pressed", self, "switch_to", [$Afflictions, $AHAB])
	$Boons/BTMContainer/BTMButton.connect("pressed", self, "switch_to", [$Boons, $AHAB])

func switch_to(fromElem: Control, toElem: Control):
	fromElem.visible = false
	toElem.visible = true