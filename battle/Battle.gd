extends Node2D
class_name Battle

signal battle_done(game_over, rewards)

var BattleCard: Resource = load("res://battle/Components/BattleCard.tscn")
var BattleSprite: Resource = load("res://battle/Components/BattleSprite.tscn")
var BattleMenu: Resource = load("res://battle/Components/PlayerBattleMenu.tscn")
var TargetPanel: Resource = load("res://battle/Components/TargetPanel.tscn")
var ReadyBar: Resource = load("res://battle/Components/ReadyBar.tscn")

var lefties: Array
var righties: Array
var registry: Dictionary

var q: Array
var player_q: Array
var curr_player: BattleSprite = null

# Called when the node enters the scene tree for the first time.
func setup(left_party: Array, right_party: Array, background: Texture = null):
	registry = {}
	lefties = left_party
	righties = right_party
	
	if background != null:
		$Background.texture = background
	
	var pos = 1
	for b in lefties:
		_init_components(b, pos, true)
		pos += 1
	pos = 0
	for b in righties:
		_init_components(b, pos, false)
		pos += 1

func _init_components(b: Battler, pos: int, is_left: bool) -> void:
	add_child(b)
	b.init_battle(pos)

	var components = {consts.BATTLER_NAME:b._name}
	_create_battle_sprite(components, is_left)
	_create_target_panel(components, is_left)
	if is_left:
		_create_battle_card(components)
		_create_battle_menu(components)

	b.set_components(components)

func _create_battle_sprite(components: Dictionary, is_left: bool):
	var bs: BattleSprite = BattleSprite.instance()
	bs.name = bs.NAME_FMT % components.get(consts.BATTLER_NAME)
	components[consts.BATTLE_SPRITE] = bs
	if is_left:
		$Left.add_sprite(bs)
	else:
		bs._flip_sprite()
		$Right.add_sprite(bs)

func _create_target_panel(components: Dictionary, is_left: bool):
	var tp = TargetPanel.instance() as TargetPanel
	tp.name = tp.NAME_FMT % components.get(consts.BATTLER_NAME)
	components[consts.TARGET_PANEL] = tp
	if is_left:
		$Ui/Tray/TargetList/ScrollContainer/MarginContainer/Lefties.add_child(tp)
	else:
		$Ui/Tray/TargetList/ScrollContainer/MarginContainer/Righties.add_child(tp)	

func _create_battle_card(components: Dictionary):
	var bc = BattleCard.instance() as BattleCard
	bc.name = bc.NAME_FMT % components.get(consts.BATTLER_NAME)
	components[consts.BATTLE_CARD] = bc
	$Ui/Tray/PartyTabs/Overview/CenterContainer/PartyList.add_child(bc)
	bc.connect("bc_pressed", self, "ttb_pressed", [components])

func ttb_pressed(components: Dictionary):
	var pt: TabContainer = $Ui/Tray/PartyTabs
	var tab_name = components.get(consts.BATTLE_MENU).name
	for i in range(0, pt.get_tab_count()):
		if pt.get_tab_title(i) == tab_name:
			pt.current_tab = i
			return
	print("not able to find", tab_name, "in tabs")

func _create_battle_menu(components: Dictionary):
	var bm = BattleMenu.instance()
	bm.name = bm.NAME_FMT % components.get(consts.BATTLER_NAME)
	bm.connect("return_to_overview", self, "bb_pressed")
	components[consts.BATTLE_MENU] = bm
	$Ui/Tray/PartyTabs.add_child(bm)

func bb_pressed():
	var pt: TabContainer = $Ui/Tray/PartyTabs
	pt.current_tab = 0

func _process(delta: float) -> void:
	if _battle_done():
		emit_signal("battle_done", _is_game_over(), _calculate_rewards())

func _battle_done() -> bool:
	# todo: check if all of either side is dead
	return false

func _is_game_over():
	for b in lefties:
		pass # todo: check if all lefties are dead
		
func _calculate_rewards():
	# todo: determine combat difficulty and generate rewards from loot table
	pass