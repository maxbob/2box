extends Node2D

var battleRes: Resource = load("res://battle/Battle.tscn")

func _ready():
	randomize()
	var lefties = []
	for i in range(int(rand_range(1, 5))):
		lefties.append(new_battler())
	var righties = []
	for i in range(int(rand_range(2, 5))):
		righties.append(new_battler())
	var battle: Battle = battleRes.instance()
	battle.setup(lefties, righties)
	self.add_child(battle)
	
func new_battler() -> Battler:
	return Battler.new()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
